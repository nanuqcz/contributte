<?php

namespace App\Presenters;

use Nette;
use Nette\Database\Context;
use Nette\Forms;
use Ublaboo\DataGrid\DataGrid;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @inject @var Context */
    public $db;


    /*
     * Sample database: https://github.com/phpcontrols/phpgrid-project-management/blob/master/db/simple_pm_install.sql
     */
    public function createComponentGrid($name)
    {
        $grid = new DataGrid($this, $name);

        $grid->setDataSource($this->db->table('projects'));

        $grid->addColumnText('ProjectName', 'Project name');
        $grid->addColumnText('StartDate', 'Start date');

        $grid->addInlineAdd()->onControlAdd[] = function (Forms\Container $container) {
            $container->addText('ProjectName', '');
            $container->addText('StartDate', '');
        };
    }

}
